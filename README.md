This is mostly a bunch of fish shell functions. There's also a file with my git aliases.  
https://fishshell.com/

I have the following fish packages:  
[Fisher](https://github.com/jorgebucaran/fisher) as a package manager. 
[Bass](https://github.com/edc/bass) to make using bash utilities & scripts easy  
[Bob the Fish](https://github.com/oh-my-fish/theme-bobthefish) to have a nicer prompt with reminders of what git branch I'm working on.  
	fisher add github.com/oh-my-fish/theme-bobthefish
and the [powerline fonts](https://powerline.readthedocs.io/en/master/installation.html#patched-fonts).  

I also have xclip installed, to allow copying data from the command line to & from the clipboard.
[TheFuck](https://github.com/nvbn/thefuck) is a handy (if not-entirely-safe-for-work-named) script to correct mistyped commands. There's a fish helper for it.
