#! /usr/bin/fish
# https://github.com/nvbn/thefuck
# The Fuck is a magnificent app,
# inspired by a @liamosaur tweet,
# that corrects errors in previous console commands.
function fuck
    eval (thefuck (history | head -n1))
end
