#! /usr/bin/fish
# Runs a docker test with the specified argument.
function docker_test
	docker_run test $argv
end

