#! /usr/bin/fish
# Runs a docker build with the selected argument.
function docker_build
	docker_clean $argv
	docker_run build $argv
end

