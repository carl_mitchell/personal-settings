#! /usr/bin/fish
# Changes to the lbb sdk folder & runs 
# source setup_env mcu repo
function mcu
    sdk
    if test -z "$argv"
        bass source setup_env mcu develop
    else
        bass source setup_env mcu $argv
    end
end