#! /usr/bin/fish
# https://www.xkcd.com/1168/
# x = extract
# z = g(un)zip
# v = verbose
# f = operate on the filename which appears next in the command line.
function untar
	tar -xzvf $argv
end

