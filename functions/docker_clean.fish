#! /usr/bin/fish
# Cleans a given docker target

function docker_clean
    if test "$argv" = "all"
        if test "$PLATFORM" = "mcu"
            docker_run clean mcu
            docker_run clean common
        else if test "$PLATFORM" = "eld"
            docker_run clean common
            docker_run clean libs
            docker_run clean services
        end
    else
        docker_run clean $argv
    end
end