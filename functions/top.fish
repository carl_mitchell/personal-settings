#! /usr/bin/fish
# Runs htop instead of top. htop is simply better.
function top
	htop
end

