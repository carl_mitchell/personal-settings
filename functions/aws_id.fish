#! /usr/bin/fish
# get the AWS ID of a certificate

function aws_id
    openssl x509 -in $argv -outform DER | sha256sum
end