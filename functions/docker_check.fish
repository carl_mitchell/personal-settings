#! /usr/bin/fish
# Runs a docker check with the selected argument.
function docker_check
	docker_run cppcheck $argv
end