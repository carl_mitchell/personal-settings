#! /usr/bin/fish
# Pings the destination 5 times, instead of indefinitely.
function  ping
	/bin/ping -c 5 $argv
end

