#! /usr/bin/fish
# Nanopb call helper
function nanopb
    protoc --plugin=protoc-gen-nanopb=/home/carl/k2labs/nanopb/generator/protoc-gen-nanopb --nanopb_out=. $argv
end