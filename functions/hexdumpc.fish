#! /usr/bin/fish
# Hexdump C array style
function hexdumpc
    command hexdump -v -e '16/1 "0x%02X, " "\n"' $argv
end