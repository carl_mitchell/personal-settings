#! /usr/bin/fish
# Runs docker with the specified argument.
function docker_run
	sdk
	tools/docker/docker_run.sh $argv
end

