#! /usr/bin/fish
# Adds everything to git & shows status.
function addall
    git add -A
    git status
end
