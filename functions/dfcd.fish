#! /usr/bin/fish

# Deep Fuzzy Change Directory
function dfcd
    br --only-folders --cmd "$argv :cd"
end