#! /usr/bin/fish
# Runs the ST-Link GDB debugger
function st_debug
    cd /opt/Atollic_TrueSTUDIO_for_STM32_x86_64_9.1.0/Servers/ST-LINK_gdbserver
    ./ST-LINK_gdbserver.sh
end