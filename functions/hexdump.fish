#! /usr/bin/fish
# Hexdump 16 bytes/line, space separated, upper-case
function hexdump
    command hexdump -v -e '"" 16/1 "%02X " "\n"' $argv
end