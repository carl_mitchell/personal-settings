#! /usr/bin/fish
# Wrap some OS utilities
function debian
    if test $argv = "--version"
        cat /etc/os-release
    else
        echo "Supported commands:"
        echo "--version"
    end
end