#! /usr/bin/fish
# Changes to the lbb sdk folder & runs 
# source setup_env develop
function eld
    sdk
    if test -z "$argv"
        bass source setup_env eld develop
    else
        bass source setup_env eld $argv
    end
end

