#! /usr/bin/fish
# Runs hexdump with byte-wise output, no newlines, no offset
function hex
    hexdump -e '/1 "%02X "' $argv
end